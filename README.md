# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-cli-providers-node-kit-project-generator [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The kwaeri/node-kit-project-generator component for the @kwaeri/node-kit application platform

[![pipeline status](https://gitlab.com/kwaeri/cli/providers/node-kit-project-generator/badges/master/pipeline.svg)](https://gitlab.com/kwaeri/cli/providers/node-kit-project-generator/commits/master)  [![coverage report](https://gitlab.com/kwaeri/cli/providers/node-kit-project-generator/badges/master/coverage.svg)](https://kwaeri.gitlab.io/cli/providers/node-kit-project-generator/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The node-kit-project-generator provides the methods for fulfilling the content creation requests of end users of kwaeri/cli [nkm].

kwaeri/node-kit-project-generator was originally included in the entry point to the platform under the directory [from root] `node-kit/core/`. In an attempt to standardize implementation - by separating concerns - as well as to ease maintainence of each component of the platform, the node-kit-project-generator was migrated to its own component module.

## Getting Started

**NOTE**

kwaeri/node-kit-project-generator is not ready for production. This module has been published for testing and development purposes. You're free to try out anything that may already be available, but please be aware that there is likely to be many aspects of the platform which are not working correctly and/or at all. As completion of the new platform and tooling nears completion, updated documentation and complete examples will be made available.

### Installation

[kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps a myriad of node-kit components under the kwaeri scope specific to the MV(a)C application framework, and provides a single entry point to the framework for easing the process of building a kwaeri application.

[kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the myriad of components under the kwaeri scope specific to the user executable framework, and provides a single entry point to the user executable (CLI).

However, to install kwaeri/node-kit-project-generator specifically:

```bash
npm install @kwaeri/node-kit-project-generator
```

### Usage

The node-kit-project-generator component is leveraged by the user executable, through the Steward, explicitly in its handling of commands passed to it by the end user requesting content to be generated - which is pretty much the core purpose of the CLI.

To make use of the Project Generator as a service provider to the user executable, no extra steps are required beyond simply installing the user executable - or CLI - itself; Globally of course so that it is made callable from anywhere:

```bash
npm install -g @kwaeri/cli
```

Once complete, the `kue` command becomes available.

Service Providers publish contracts that can be consumed by subscribers (in our case, the end user). The published service provider subscriptions come in the form of commands that the CLI supports for the end user. To get a list of available commands at any time, run:

```bash
kue --help
```

To see a list of specifications for a command, run:

```bash
kue <command> --help
```

After installing kwaeri/node-kit-project-generator and configuring it, the `new` command becomes available (if it wasn't already made so by other service providers), as does the `project` specification (and, again).

The types of projects that kwaeri/node-kit-project-generator supports are:

* Kwaeri REST (Based on NodeKit MVC)
* MVC (REST with a Renderer, featuring a HTML template engine)
* React Client-side Application
* Coming soon
  * XRM

The NodeKit service providers are the default providers, which are made available automatically if no other service providers are installed and/or configured (i.e. there is no CLI configuration). You can configure any service provider explicitly - including the NodeKit service providers, which are explicitly configured in a NodeKit Template Project - by adding them to the providers array within the CLI configuration.

The CLI configuration can live in various places, with standard scoping rules applied as follows (descending in specificity):

* Project Root (i.e. `./`)
* Project configuration directory (i.e. `./conf`)
* User directory (i.e. `~/)

If in the root of the repository, the convention is to name it `cli.json`. However, when anywhere else the convention is to name the file according to the environment its intended to be used in. For example:

* `cli.default.json` as a fallback for any environment.
* `cli.production.json` when the environment *(`NODE_ENV=production`)* is for production.
* `cli.test.json` when the environment *(`NODE_ENV=test`)* is for testing.

In many cases, when you use the CLI in automation, you'll need for progress bars, and other mechanics, to be disabled as many CI environments lack a TTY to output to. In these circumstances, you can leverage the test environment to assure a passing pipeline (i.e. run `export NODE_ENV=test` in your script before the process that leverages the project generator, and the progress bar and its notification and logging system will be disabled.)

When you're finally ready to go, you can generate a new project of the available types by running one single command:

```bash
kue new project --type <type> <Project Name>
```

A directory for the project will be created in the current working directory, with the name of the directory transformed from what was provided into a file-safe name (i.e. `My Test REST Project` becomes `my-test-rest-project`.)

```bash
kue new project --type REST "My Test REST Project"
```

`MyTestRestProject` also becomes `my-test-rest-project` so as to - in order to additionally support transforming resource name to usable class name - preserve camel casing. In such cases as when a word like `MySQL` is used it is important to note that the `SQL` portion will not have its capitalization preserved after transformation. The conversion doesn't transform it into `my-s-q-l-` as a file/class safe replacement because it searches for words that start with one uppercase, not just an uppercase letter. `MySQLMigration` becomes `my-sql-migration` as intended, and which is part of the design. The class name constructed from the resulting file name would be `MySqlMigration`. For this reason it's better to leverage `Mysql Migration` as a starting name, for a filename of `mysql-migration` and a class name of `MysqlMigration`. Though, and long as you are able to properly anticipate the expected result - the design choice is up to you.

More to come...

#### Programatically

After installation, one would need to import it:

```typescript
import { NodeKitProjectGenerator } from '@kwaeri/node-kit-project-generator';
```

And instantiate it:

```typescript
const progress = generator = new NodeKitProjectGenerator();
```

For insight on how you might manipulate the module programmatically you could review the tests available in the repository.

To be continued...

**NOTE**

As mentioned earlier, the plan is to continue development of the myriad components of the node-kit platform - the node-kit-project-generator component included - and ultimately ease the process of development, maintainence, and usage of each individual component as they are decoupled from one another.


## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/groups/kwaeri/node-kit/-/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [Service Desk](mailto:incoming+kwaeri-node-kit-node-kit-project-generator-17965981-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/groups/kwaeri/node-kit/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
