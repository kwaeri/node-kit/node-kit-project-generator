/**
 * SPDX-PackageName: kwaeri/node-kit-project-generator
 * SPDX-PackageVersion: 0.9.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES


// ESM WRAPPER
export type {
    ProjectGeneratorOptions,
} from './src/project-generator.mjs';

export {
    PARAMATERIZATION,
    TEMPLATE_CONSTANTS,
    TEMPLATE_CONSTANT_OPTIONS,
    NodeKitProjectGenerator
} from './src/project-generator.mjs'
